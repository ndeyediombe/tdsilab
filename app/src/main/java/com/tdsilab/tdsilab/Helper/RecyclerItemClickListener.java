package com.tdsilab.tdsilab.Helper;

/**
 * Created by Moussa Diallo on 26/03/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class RecyclerItemClickListener
        implements RecyclerView.OnItemTouchListener
{
    GestureDetector mGestureDetector;
    private OnItemClickListener mListener;

    public RecyclerItemClickListener(Context paramContext, OnItemClickListener paramOnItemClickListener)
    {
        this.mListener = paramOnItemClickListener;
        this.mGestureDetector = new GestureDetector(paramContext, new GestureDetector.SimpleOnGestureListener()
        {
            public boolean onSingleTapUp(MotionEvent paramAnonymousMotionEvent)
            {
                return true;
            }
        });
    }

    public boolean onInterceptTouchEvent(RecyclerView paramRecyclerView, MotionEvent paramMotionEvent)
    {
        View localView = paramRecyclerView.findChildViewUnder(paramMotionEvent.getX(), paramMotionEvent.getY());
        if ((localView != null) && (this.mListener != null) && (this.mGestureDetector.onTouchEvent(paramMotionEvent))) {
            this.mListener.onItemClick(localView, paramRecyclerView.getChildAdapterPosition(localView));
        }
        return false;
    }

    public void onRequestDisallowInterceptTouchEvent(boolean paramBoolean) {}

    public void onTouchEvent(RecyclerView paramRecyclerView, MotionEvent paramMotionEvent) {}

    public static abstract interface OnItemClickListener
    {
        public abstract void onItemClick(View paramView, int paramInt);
    }
}
