package com.tdsilab.tdsilab.Util;

/**
 * Created by Moussa Diallo on 26/03/2017.
 */

public class LinkUrl {

    private static String adresse = "http://192.168.43.4";
    private static String port = ":80";
    private static String url_all_atelier;
    private static String folder_atelier;
    private static String folder_user;

    static {
        url_all_atelier = adresse + port + "/tdsilab/allAtelier.php";
        folder_atelier = adresse + port + "/images/atelier/";
        folder_user = adresse + port + "/images/user/";
    }

    public static String getUrl_all_atelier() {
        return url_all_atelier;
    }

    public static void setUrl_all_atelier(String url_all_atelier) {
        LinkUrl.url_all_atelier = url_all_atelier;
    }

    public static String getFolder_atelier() {
        return folder_atelier;
    }

    public static void setFolder_atelier(String folder_atelier) {
        LinkUrl.folder_atelier = folder_atelier;
    }

    public static String getFolder_user() {
        return folder_user;
    }

    public static void setFolder_user(String folder_user) {
        LinkUrl.folder_user = folder_user;
    }
}
